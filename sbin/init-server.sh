#!/bin/bash

usage() {
  local prog=$(basename "$0")
  echo "Usage: $prog <domain> "
  exit 1
}
[ $# -lt 1 -o "$1" == "" ] && usage

domain=$1
zvs_tools_path=$(dirname `readlink -f $0`)/..

#change nginx template
sed -i "s/@basedomain@/$domain/" $zvs_tools_path/templates/nginx-vhost.conf

#install all necessary software

#add line for php5-fpm
echo -e '\n\n####ADDED BY ZVS-TOOLS####\ndeb http://packages.dotdeb.org stable all\ndeb-src http://packages.dotdeb.org stable all' >> /etc/apt/sources.list
#add gpg key
wget http://www.dotdeb.org/dotdeb.gpg
cat dotdeb.gpg | apt-key add -


apt-get update

#this is for silent install
export DEBIAN_FRONTEND=noninteractive

#nginx
apt-get -q -y install nginx
sed -i "s/# server_names_hash_bucket_size 64;/server_names_hash_bucket_size 64;/" /etc/nginx/nginx.conf
service nginx restart

#mysql
apt-get -q -y install mysql-server
MYSQLPASS="$(cat /dev/urandom | tr -cd "[:alnum:]" | head -c 10)"
mysqladmin -u root password "$MYSQLPASS"
echo -e "[mysql]\nuser=root\npassword=$MYSQLPASS" > /root/.my.cnf

#PHP
apt-get -q -y install php5 php5-fpm php5-mysql php5-pgsql php5-suhosin php5-curl php5-gd php5-mcrypt

#misc
apt-get -q -y install sudo drush


#PHP xhprof
#apt-get install -q -y php5-xhprof
