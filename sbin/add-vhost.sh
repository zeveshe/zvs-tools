#!/bin/bash
#set -x
usage() {
  local prog=$(basename "$0")
  echo "Usage: $prog <username> "
  exit 1
}
[ $# -lt 1 -o "$1" == "" ] && usage

zvs_tools_path=$(dirname `readlink -f $0`)/..

username="$1"

username=$(builtin printf '%q' "$username")

#new user
password="$(cat /dev/urandom | tr -cd "[:alnum:]" | head -c 10)"
pass_crypted=$(perl -e 'print crypt($ARGV[0], "password")' $password)
useradd -m -s /bin/bash -p "$pass_crypted" $username

#create dirs
chmod -R 770 /home/$username
mkdir /home/$username/logs
mkdir /home/$username/www
chown    $username:www-data /home/$username
chown -R $username:www-data /home/$username/www
chown -R $username:www-data /home/$username/logs

#nginx conf
cp -f $zvs_tools_path/templates/nginx-vhost.conf  /etc/nginx/sites-available/$username.conf
sed -i "s/@username@/$username/g" /etc/nginx/sites-available/$username.conf
ln -s /etc/nginx/sites-available/$username.conf /etc/nginx/sites-enabled/$username.conf
service nginx reload

#php5-fpm conf
cp -f $zvs_tools_path/templates/php5-fpm-pool.conf  /etc/php5/fpm/pool.d/$username.conf
sed -i "s/@username@/$username/g" /etc/php5/fpm/pool.d/$username.conf
service php5-fpm restart

#mysql db
mysql -e "create database $username;"
MYSQLPASS="$(cat /dev/urandom | tr -cd "[:alnum:]" | head -c 10)"
mysql -e "grant all privileges on $username.* to $username@'localhost' identified by '$MYSQLPASS';"
sudo -u $username echo -e "[mysql]\nuser=$username\npassword=$MYSQLPASS" > /home/$username/.my.cnf

chown $username:$username /home/$username/.my.cnf
chmod 700 /home/$username/.my.cnf

#create test index.php
sudo -u $username echo -e "<?php echo \"$username\";\nphpinfo();" > /home/$username/www/index.php

echo -e "\n###ALL FINISHED!###\n"
echo -e "###MYSQL CREDENTIALS###\nuser=$username\npassword=$MYSQLPASS\ndatabase=$username\n"
echo -e "###USER CREDENTIALS###\nuser=$username\npassword=$password\n"
